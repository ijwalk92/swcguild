function startGame() {
	
	var startBet = (document.getElementById("startingBet").value);
	var startBetValue = Number(startBet);
	var rollSum = 0;
	var totalRolls = 0;
	var totalWins = 0;
	var maxMoney = startBetValue;
	var maxMoneyRolls = 0;
	var displayResults = document.getElementById("displayResults");

	if (startBetValue < 1) {
		
		return alert("One dollar to play, or you go away!");
		
	} else if (isNaN(startBet)) {
		
		return alert("I pay bills with real numbers only!")
		
	} else if (startBet % 1 !== 0){
		
		return alert("Keep the change, ya filthy animal!");
		
	} else {
		
		playGame();
		
	}
	
	function playGame() {
		
		while (startBetValue >= 1) {
			
			var die1 = Math.floor(Math.random() * 6) + 1;
			var die2 = Math.floor(Math.random() * 6) + 1;
			
			rollSum = (die1 + die2);
			
			totalRolls++;
			
			winOrLose();
			results();
			playAgain();
			
		}
		
	}
	
	function winOrLose() {
		
		if (rollSum == 7) {
			
			startBetValue += 4;
			totalWins++;
			
			if (startBetValue > maxMoney) {
			
				maxMoney = startBetValue;
				maxMoneyRolls = totalRolls;
			
			}
			
		} else {
			
			startBetValue -= 1;
			
		}

	}
	
	function results() {
		
		document.getElementById("resultStartingBet").innerHTML = "$" + (Number(startBet)).toFixed(2);
		document.getElementById("resultTotalRolls").innerHTML = totalRolls;
		document.getElementById("resultMaxMoney").innerHTML = "$" + (Number(maxMoney)).toFixed(2);
		document.getElementById("resultMaxMoneyRoll").innerHTML = maxMoneyRolls;
		
		displayResults.style.visibility = "visible";
		
	}
	
	function playAgain() {
		
		document.getElementById("luckyButton").innerHTML = "Play Again?";
		
	}
	

}