function contactValidation() {
	
	var name = ((document.getElementById("name").value).trim());
	var email = ((document.getElementById("email").value).trim());
	var number = ((document.getElementById("number").value).trim());
	var reason = (document.getElementById("reason").value);
	var addInfo = ((document.getElementById("addInfo").value).trim());
	var days = [(document.getElementById("M").checked),(document.getElementById("T").checked),(document.getElementById("W").checked),(document.getElementById("TH").checked),(document.getElementById("F").checked)];
	
	return nameValidate();
	
	
	function nameValidate() {
		
		if ((name == null) || (name == "")) {
		
			alert("name is a required field");
			return false;
		
		} else {

			return emailOrNumber();
			
		}
		
	}
	
	function emailOrNumber() {
		
		if (((email == null) || (email == "")) && ((number == null) || (number == ""))) {
		
			alert("A valid email address or phone number is required");
			return false;
		
		} else {

			return reasonDetails();
			
		}
		
	}
	
	function reasonDetails() {
		
		if ((reason == "OTHER") && ((addInfo == null) || (addInfo == ""))) {
	
			alert("Please include the details of your inquiry");
			return false;
			
		} else {

			return bestContactDay();
			
		}
		
	}

	
	function bestContactDay() {
		
		
		if (days.indexOf(true) == -1) {
		
			alert("please select best day(s) to contact you");
			return false;
		
		} else {
		
			return true;
	
		}
		
	}
	
}
